# 需要更多的选择

> 一旦我们放弃自己选择的能力，别的力量或者别人就会插手替我们做出选择。

举个**挑西瓜**的例子。老板，帮我挑一个甜点的习惯吧！对卖西瓜老板来说，目的是把甜的和不甜的都卖出去，遇到傻子你觉得他可能把好的西瓜留个你么？

### 如何实现更多选择？

+ 找到更多的可选项
+ 选择做某事，也可以选择不做或者做的更少(斗牛犬餐厅)
+ 可以随时改变原来的选择(沉默成本)

